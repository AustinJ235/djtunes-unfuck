extern crate metaflac;

use std::fs;
use std::io::Read;
use std::collections::HashMap;

fn main() {
	let files = get_file_infos();
	let inputs = read_input();
	
	let mut input_map = HashMap::new();
	for input in inputs {
		let dumb_artist = dumb_string(&input.artist).to_lowercase();
		let dumb_title = dumb_string(&input.title).to_lowercase();
		let key = format!("{} {}.flac", dumb_artist, dumb_title);
		input_map.insert(key, input);
	}
	
	for file in files {
		let input = match input_map.get(&file.file_name) {
			Some(some) => some,
			None => {
				println!("Failed to find input for file {}", file.path);
				continue;
			}
		}; 
		
		let mut tag = metaflac::Tag::read_from_path(&file.path).unwrap();

		tag.set_vorbis("ARTIST", vec![input.artist.clone()]);
		tag.set_vorbis("TITLE", vec![input.title.clone()]);
		tag.set_vorbis("TRACKNUMBER", vec![format!("{}", input.num)]);
		tag.set_vorbis("ALBUM", vec![file.album]);
		
		tag.save();
	}
}

fn dumb_string(input: &String) -> String {
	input
		.replace("&", " ")
		.replace("''", " ")
		.replace("'", " ")
}

#[derive(Debug)]
struct InputSong {
	num: u32,
	title: String,
	artist: String,
}

fn read_input() -> Vec<InputSong> {
	let mut handle = fs::File::open("./data/input.txt").unwrap();
	let mut data = String::new();
	handle.read_to_string(&mut data).unwrap();
	
	let mut i = 0_u8;
	let mut exp_num = 1_u32;
	let mut cur_line = 0_usize;
	let mut out = Vec::new();
	
	let mut title = String::new();
	let mut artist = String::new();
	
	for line in data.lines() {
		if i == 0 {
			let split_by = format!("{}", exp_num);
			let mut iter = line.split(split_by.as_str());
			
			let num_str = match iter.next() {
				Some(some) => some,
				None => panic!("Line {}: Expected number not found!", cur_line + 1)
			};
			
			if num_str.len() > 0 {
				panic!("Line {}: Characters before expected number!", cur_line + 1);
			}
			
			title = String::new();
			
			for item in iter {
				title.push_str(item);
				title.push_str(format!("{}", exp_num).as_str());
			} if title.len() < 1 {
				panic!("Line {}: Expected title after number!", cur_line + 1);
			}
			
			title = title.split_at(title.len() - split_by.len()).0.to_string();
			i = 1;
		} else if i == 1 {
			artist = line.to_string();
			i = 2;
		} else if i == 2 {
			if line != "Download" {
				panic!("Line {}: Expected word 'Download'", cur_line + 1);
			}
			
			out.push(InputSong {
				num: exp_num.clone(),
				title: title.clone(),
				artist: artist.clone()
			});
			
			exp_num += 1;
			i = 0;
		} cur_line += 1;
	}
	
	out
}

#[derive(Debug)]
struct FileInfo {
	pub album: String,
	pub path: String,
	pub file_name: String,
}

fn get_file_infos() -> Vec<FileInfo> {
	let mut out = Vec::new();
	for item_ in fs::read_dir("./data/music/").unwrap() {
		let item = item_.unwrap();
		
		if item.metadata().unwrap().is_dir() {
			let album = item.file_name().into_string().unwrap();
			let scan_path = format!("./data/music/{}", album);
			
			for dir_item_ in fs::read_dir(scan_path.as_str()).unwrap() {
				let dir_item = dir_item_.unwrap();
				
				if dir_item.metadata().unwrap().is_file() {
					out.push(FileInfo {
						album: album.clone(),
						file_name: dir_item.file_name().into_string().unwrap(),
						path: format!("./data/music/{}/{}", album, dir_item.file_name().into_string().unwrap()),
					});
				}
			}
		}
	} out
}